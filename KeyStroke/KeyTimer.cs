﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStroke
{
    class KeyTimer
    {
        public int key { get; set; }
        public int elapsedTime { get; set; }
    }
}
