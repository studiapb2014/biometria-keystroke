﻿using KeyStroke.Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeyStroke
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Calculating keystroke;
        Stopwatch stopwatch;
        Stopwatch stopwatchDelay;
        List<KeyTimer> keys;
        int globalDelay = 0;
        int globalDelayCount = 0;

        int globalSpaceFirst = 0;
        int globalSpaceFirstCount = 0;

        int globalSpaceSecond = 0;
        int globalSpaceSecondCount = 0;

        Key lastKey;
        public MainWindow()
        {
            InitializeComponent();
            keystroke = new Calculating();
            keys = new List<KeyTimer>();

           
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            keystroke.FirstFeatureVector();
            MessageBox.Show("Zrobiono pierwszy wektor cech: C:\fv1.csv");
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            keystroke.SecondFeatureVector();
            MessageBox.Show("Zrobiono drugi wektor cech: C:\fv2.csv");
        }

        private void richTextBox_KeyDown(object sender, KeyEventArgs e)
        {

            stopwatch = new Stopwatch();
            stopwatch.Start();
            stopwatchDelay.Stop();
            globalDelay +=(int) stopwatchDelay.ElapsedMilliseconds;
            globalDelayCount++;

        }

        private void richTextBox_KeyUp(object sender, KeyEventArgs e)
        {  
            stopwatch.Stop();

            Key key = e.Key;
            KeyTimer keyTimer = new KeyTimer();
            string keyS = key.ToString();
            char keyCh = keyS[0];
            keyTimer.key = (int)keyCh;

            keyTimer.elapsedTime =(int)stopwatch.ElapsedMilliseconds;
            keys.Add(keyTimer);

            int[] counter = { -1, -1, -1 };
            int.TryParse(txtBox.Text, out counter[0]);
            int.TryParse(txtBox2.Text, out counter[1]);
            int.TryParse(txtBox3.Text, out counter[2]);
            if (keys.Count == counter[0] || keys.Count == counter[1] || keys.Count == counter[2])
            {
                button2.IsEnabled = true;
                CheckFeatureVector();
            }
       

            stopwatchDelay = new Stopwatch();
            stopwatchDelay.Start();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            keys = new List<KeyTimer>();
        }

        void CheckFeatureVector()
        {
            
            if (keystroke.userList.Count == 0) keystroke.FirstFeatureVector();
            List<User> users = keystroke.userList;


            User rootUser = keystroke.FirstFreatureVectorFromList(keys);
            foreach (var user in users)
            {
                user.Fv1Compare = keystroke.CompareVectors(rootUser, user);

            }
            users = users.OrderBy(o => o.Fv1Compare).ToList();
            List<User> gridList;
            gridList = users.Take(3).ToList();

            var bindingList = new BindingList<User>(gridList);
          
            dataGrid.ItemsSource = gridList;
        }

    }
}
