﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStroke
{
    class User
    {
        public int id { get; set; }
        public int [] FeatureVector1 { get; set; }
        public double Fv1Compare { get; set; }
        public int[] FeatureVector2 { get; set; }
        public double Fv2Compare { get; set; }
    }
}
