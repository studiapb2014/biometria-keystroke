﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeyStroke.Code
{
    class Calculating
    {
        SQLtoLinqConnection db;
        public List<User> userList { get; }

        public Calculating()
        {
            db = new SQLtoLinqConnection();
            userList = new List<User>();
        }

        public void FirstFeatureVector()
        {
            var ids = db.tx_badanie01s.Select(m => m.user_id).Distinct();
            foreach (var id in ids)
            {
                int[] diffPressed = new int[255];
                int[] count = new int[255];

                int[] timeDown = new int[255];
                int[] timeUp = new int[255];
                Array.Clear(diffPressed, 0, 255);
                Array.Clear(count, 0, 255);
                var u = db.tx_badanie01s.Select(m => m).Where(m => m.user_id == id);
                foreach (var row in u)
                {
                    List<string> input0 = row.input0.Split(' ').ToList();
                    List<string> input1 = row.input1.Split(' ').ToList();

                    for (int i = 0; i < input0.Count(); i++)
                    {
                        List<string> splitted = input0[i].Split('_').ToList();

                        if (splitted[0] == "d")
                        {
                            timeDown[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                        }
                        else if (splitted[0] == "u")
                        {
                            timeUp[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                            diffPressed[int.Parse(splitted[1])] += timeUp[int.Parse(splitted[1])] - timeDown[int.Parse(splitted[1])];
                            count[int.Parse(splitted[1])]++;
                        }
                    }
                    for (int i = 0; i < input1.Count(); i++)
                    {
                        List<string> splitted = input1[i].Split('_').ToList();

                        if (splitted[0] == "d")
                        {
                            timeDown[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                        }
                        else if (splitted[0] == "u")
                        {
                            timeUp[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                            diffPressed[int.Parse(splitted[1])] += timeUp[int.Parse(splitted[1])] - timeDown[int.Parse(splitted[1])];
                            count[int.Parse(splitted[1])]++;
                        }
                    }



                }
                for (int i = 0; i < count.Length; i++)
                {
                    if (count[i] != 0)
                    {
                        diffPressed[i] /= count[i];

                    }
                }
                AddLineToCSVFirst("fv1", (int)id, diffPressed);
                User user = new User();
                user.id = (int)id;
                user.FeatureVector1 = diffPressed;
                userList.Add(user);
            }
        }
        public void SecondFeatureVector()
        {
            var ids = db.tx_badanie01s.Select(m => m.user_id).Distinct();
            foreach (var id in ids)
            {
                int[] diffPressed = new int[255];
                int[] count = new int[255];
                int globalCount = 0;
                int globaldiffPressed = 0;
                int[] timeDown = new int[255];
                int[] timeUp = new int[255];

                int globalPauseTime = 0;
                int globalPauseCount = 0;
                int countPressedButtons = 0;
                int pauseStart = 0;
                int pauseEnd = 0;

                int globalSpaceTimePress = 0;
                int globalSpaceCountPress = 0;
                int lastButtonTime = 0;

                int globalSpaceTimeRelease = 0;
                int globalSpaceCountRelease= 0;
                bool spaceWasPressed = false;

                Array.Clear(diffPressed, 0, 255);
                Array.Clear(count, 0, 255);
                var u = db.tx_badanie01s.Select(m => m).Where(m => m.user_id == id);
                foreach (var row in u)
                {
                    List<string> input0 = row.input0.Split(' ').ToList();
                    List<string> input1 = row.input1.Split(' ').ToList();

                    for (int i = 0; i < input0.Count(); i++)
                    {
                        List<string> splitted = input0[i].Split('_').ToList();

                        if (splitted[0] == "d")
                        {
                            timeDown[int.Parse(splitted[1])] = int.Parse(splitted[2]);

                            if (i!=0 && countPressedButtons == 0)
                            {
                                pauseEnd = int.Parse(splitted[2]);
                                globalPauseCount++;
                                globalPauseTime += pauseEnd - pauseStart;
                            }
                            countPressedButtons++;

                            //sprawdzanie wcisniecia spacji
                            if(splitted[1] == "32")
                            {
                                globalSpaceTimePress = timeDown[int.Parse(splitted[1])] - lastButtonTime;
                                globalSpaceCountPress++;
                            }

                            //sprawdzenie czy ostatnio byla wcisnieta spacja
                            if(spaceWasPressed)
                            {
                                globalSpaceTimeRelease = timeDown[int.Parse(splitted[1])] - lastButtonTime;
                                globalSpaceCountRelease++;
                            }
                        }
                        else if (splitted[0] == "u")
                        {
                            countPressedButtons--;
                            timeUp[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                            lastButtonTime = timeUp[int.Parse(splitted[1])];
                            diffPressed[int.Parse(splitted[1])] += timeUp[int.Parse(splitted[1])] - timeDown[int.Parse(splitted[1])];
                            count[int.Parse(splitted[1])]++;

                            if (countPressedButtons == 0)
                            {
                                pauseStart = int.Parse(splitted[2]);
                            }
                            if (splitted[1] == "32")
                            {
                                spaceWasPressed = true;
                            }
                            else spaceWasPressed = false;


                        }
                        
                    }
                    for (int i = 0; i < input1.Count(); i++) // sprawdzanie drugiego inputa
                    {
                        List<string> splitted = input1[i].Split('_').ToList();

                        if (splitted[0] == "d")
                        {
                            timeDown[int.Parse(splitted[1])] = int.Parse(splitted[2]);

                            if (i != 0 && countPressedButtons == 0)
                            {
                                pauseEnd = int.Parse(splitted[2]);
                                globalPauseCount++;
                                globalPauseTime += pauseEnd - pauseStart;
                            }
                            countPressedButtons++;

                            //sprawdzanie wcisniecia spacji
                            if (splitted[1] == "32")
                            {
                                globalSpaceTimePress = timeDown[int.Parse(splitted[1])] - lastButtonTime;
                                globalSpaceCountPress++;
                            }

                            //sprawdzenie czy ostatnio byla wcisnieta spacja
                            if (spaceWasPressed)
                            {
                                globalSpaceTimeRelease = timeDown[int.Parse(splitted[1])] - lastButtonTime;
                                globalSpaceCountRelease++;
                            }
                        }
                        else if (splitted[0] == "u")
                        {
                            countPressedButtons--;
                            timeUp[int.Parse(splitted[1])] = int.Parse(splitted[2]);
                            lastButtonTime = timeUp[int.Parse(splitted[1])];

                            diffPressed[int.Parse(splitted[1])] += timeUp[int.Parse(splitted[1])] - timeDown[int.Parse(splitted[1])];
                            count[int.Parse(splitted[1])]++;

                            if (countPressedButtons == 0)
                            {
                                pauseStart = int.Parse(splitted[2]);
                            }
                            if (splitted[1] == "32")
                            {
                                spaceWasPressed = true;
                            }
                            else spaceWasPressed = false;

                        }
                    }

                }


                for (int i = 33; i < 127; i++) // sprawdzanie tylko normalnych przycisków
                {
                    if (count[i] != 0)
                    {
                        globaldiffPressed += diffPressed[i];
                        globalCount += count[i];
                    }
                }
                if (globalCount != 0) globaldiffPressed /= globalCount;
                if (globalPauseCount != 0) globalPauseTime /= globalPauseCount;
                if (globalSpaceCountPress != 0) globalSpaceTimePress /= globalSpaceCountPress;
                if (globalSpaceCountRelease != 0) globalSpaceTimeRelease /= globalSpaceCountRelease;

                int[] featureVector = new int[4] { globaldiffPressed, globalPauseTime, globalSpaceTimePress, globalSpaceTimeRelease };

                AddLinetoCSVSecondFeatureVector("fv2.1", (int)id, featureVector);
                foreach (var item in userList.Where(x=> x.id == id) )
                {
                    item.FeatureVector2 = featureVector;
                } 
                  
            }
        }

        public void AddLineToCSVFirst(string filename, int id, int[] featureVector)
        {
            File.AppendAllText(@"c:\" + filename + ".csv", id.ToString() + ", ");
            for (int i = 65; i < 91; i++)
            {
              //  if (featureVector[i] != 0)
                {
                    string s = (Convert.ToChar(i)).ToString();
                    File.AppendAllText(@"c:\" + filename + ".csv",  featureVector[i].ToString() + ", ");
                }

            }
            File.AppendAllText(@"c:\" + filename + ".csv", Environment.NewLine);
        }
        public void AddLinetoCSVSecondFeatureVector(string filename, int id, int[] featureVector)
        {
            File.AppendAllText(@"c:\" + filename + ".csv", id.ToString() + ", ");
            for (int i = 0; i < 4; i++)
            {
                    File.AppendAllText(@"c:\" + filename + ".csv", featureVector[i].ToString() + ", ");

            }
            File.AppendAllText(@"c:\" + filename + ".csv", Environment.NewLine);
        }

        public User FirstFreatureVectorFromList(List<KeyTimer> keys)
        {
            int[] diffPressed = new int[255];
            int[] count = new int[255];
            Array.Clear(diffPressed, 0, 255);
            Array.Clear(count, 0, 255);
            foreach (var key in keys)
            {
                diffPressed[key.key] += key.elapsedTime;
                count[key.key]++;
            }
            for (int i = 0; i < count.Length; i++)
            {
                if (count[i] != 0)
                {
                    diffPressed[i] /= count[i];

                }
            }

            User user = new User();
            user.id = -1;
            user.FeatureVector1 = diffPressed;
            return user;
        }

        public double CompareVectors(User user1, User user2)
        {
            double sum = 0;
            for (int i = 33; i < 127; i++)
            {
              sum+= ( Math.Pow((user1.FeatureVector1[i] - user2.FeatureVector1[i]),2));
            }
            return sum = Math.Sqrt(sum);
        }
    }
}
